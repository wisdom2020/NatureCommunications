star=/opt/NGS/STAR/STAR-2.5.4b/bin/Linux_x86_64_static/STAR   
dir_fasta=/data1/Annotation/iGenome/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa
root=/data1/workspace/DCI/Kirsch/Yvonne.Mowery/RNASeq-WES/RNASeq
gtf=${root}/Data/Annotation/gencode.vM17.annotation.gtf
dir_star_idx=${root}/Results/STAR-Index

$star --runMode genomeGenerate \
      --runThreadN 8 \
      --genomeDir $dir_star_idx \
      --genomeFastaFiles $dir_fasta \
      --sjdbGTFfile $gtf \
      --sjdbOverhang 100

cp $gtf ${dir_star_idx}/