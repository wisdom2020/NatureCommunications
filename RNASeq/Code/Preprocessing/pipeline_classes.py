import re


# Cmd instances represent single commands + options
# special attributes (cmd, input, output, output_suffix, pipe, err)
# are set on instantiation
class Cmd:
    def __init__(self, **kwargs):
        self.cmd = False
        self.input = False
        self.output = False
        self.output_suffix = False
        self.pipe = False
        self.err = False

        if kwargs:
            for item in ('cmd', 'input', 'output', 'output_suffix', 'pipe', 'err'):
                self.catch(item, kwargs)

    def catch(self, key, kwargs):
        try:
            val = kwargs[key]
            setattr(self, key, val)
        except KeyError:
            pass

    def __str__(self):
        return self.cmd


nio = re.compile(r"{(?!input|output).+}")
# Dict subclass for holding environment for a cmd
# used to fill in missing values in the Cmd's cmd
class Env(dict):
    def __missing__(self, key):
        if key == "input" or key == "output":
            return "{" + key + "}"

    def __getitem__(self, key):
        val = super().__getitem__(key)
        while re.search(nio, val):
            val = val.format_map(self)

        return val

    def replace(self, cmd):
        new_cmd = str(cmd).format_map(self)

        extra = {}
        if cmd.input:
            extra["input"] = cmd.input
        if cmd.output:
            extra["output"] = cmd.output

        try:
            return new_cmd.format_map(extra).format_map(self)
        except KeyError:
            print(cmd, cmd.input, cmd.output, cmd.output_suffix)
            raise
